<?php

function custom_excerpt_length( $length ) {
	return 55;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_action( 'after_setup_theme', 'setup' );  
function setup() {  
    // ...  
    add_theme_support( 'post-thumbnails' ); // This feature enables post-thumbnail support for a theme  
    // ...  
} 

add_filter( 'image_size_names_choose', 'custom_image_sizes_choose' );  
function custom_image_sizes_choose( $sizes ) {  
    $custom_sizes = array(  
        'custom-size1' => 'Full Size',  
        'header' => 'Header'  
    );  
    return array_merge( $sizes, $custom_sizes );  
}   

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'blog-sidebar',
		'before_widget' => '',
		'after_widget' => '</dt></dl>',
		'before_title' => '<div class="accordion-title accordion-toggler">',
		'after_title' => '</div><dl class="mod-content"><dt>',
	) );
}

add_action( 'widgets_init', 'arphabet_widgets_init' );

function register_my_menus() {
  register_nav_menus(
    array( 'mobile-menu' => __( 'Mobile Menu' ),
		   'footer-menu' => __( 'Footer Menu' ),
		   'header-menu' => __( 'Header Menu' )
		 )
  );
}
add_action( 'init', 'register_my_menus' );

/**
 * Add "first" and "last" CSS classes to dynamic sidebar widgets. Also adds numeric index class for each widget (widget-1, widget-2, etc.)
 */
function widget_first_last_classes($params) {

	global $my_widget_num; // Global a counter array
	$this_id = $params[0]['id']; // Get the id for the current sidebar we're processing
	$arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets	

	if(!$my_widget_num) {// If the counter array doesn't exist, create it
		$my_widget_num = array();
	}

	if(!isset($arr_registered_widgets[$this_id]) || !is_array($arr_registered_widgets[$this_id])) { // Check if the current sidebar has no widgets
		return $params; // No widgets in this sidebar... bail early.
	}

	if(isset($my_widget_num[$this_id])) { // See if the counter array has an entry for this sidebar
		$my_widget_num[$this_id] ++;
	} else { // If not, create it starting with 1
		$my_widget_num[$this_id] = 1;
	}

	$class = 'class="widget-' . $my_widget_num[$this_id] . ' '; // Add a widget number class for additional styling options

	if($my_widget_num[$this_id] == 1) { // If this is the first widget
		$class .= 'widget-first ';
	} elseif($my_widget_num[$this_id] == count($arr_registered_widgets[$this_id])) { // If this is the last widget
		$class .= 'widget-last ';
	}

	//$params[0]['before_widget'] = str_replace('class="', $class, $params[0]['before_widget']); // Insert our new classes into "before widget"
$params[0]['before_widget'] = preg_replace('/class=\"/', "$class", $params[0]['before_widget'], 1);

	return $params;

}
add_filter('dynamic_sidebar_params','widget_first_last_classes');

function updateNumbers() {  
    global $wpdb;  
    $querystr = "SELECT $wpdb->posts.* FROM $wpdb->posts WHERE $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'post' ";  
    $pageposts = $wpdb->get_results($querystr, OBJECT);  
    $counts = 0 ;  
    if ($pageposts):  
    foreach ($pageposts as $post):  
    setup_postdata($post);  
    $counts++;  
    add_post_meta($post->ID, 'incr_number', $counts, true);  
    update_post_meta($post->ID, 'incr_number', $counts);  
    endforeach;  
    endif;  
}  
  
add_action ( 'publish_post', 'updateNumbers' );  
add_action ( 'deleted_post', 'updateNumbers' );  
add_action ( 'edit_post', 'updateNumbers' );  

/** changing default wordpres email settings */
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from_name($old) {
 return 'BethTakingPhotography';
}

/**
 * Attach a class to linked images' parent anchors
 * e.g. a img => a.img img
 */
function give_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
  $classes = 'img'; // separated by spaces, e.g. 'img image-link'

  // check if there are already classes assigned to the anchor
  if ( preg_match('/<a.*? class=".*?">/', $html) ) {
    $html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $classes . '$2', $html);
  } else {
    $html = preg_replace('/(<a.*?)>/', '$1 class="' . $classes . '" >', $html);
  }
  return $html;
}
add_filter('image_send_to_editor','give_linked_images_class',10,8);

function my_custom_post_testimonials() {
	$labels = array(
		'name' 					=> _x( 'Testimonials', 'post type general name'),
		'singular_name' 		=> _x( 'Testimonial', 'post type singular name'),
		'add_new' 				=> _x( 'Add New Testimonial', 'book'),
		'add_new_item'			=> ( 'Add New Testimonial' ),
		'edit_item'				=> ( 'Edit Testimonial' ),
		'new_item'				=> ( 'New Testimonial' ),
		'all_items'				=> ( 'All Testimonials' ),
		'view_item'				=> ( 'View Testimonial' ),
		'search_items'			=> ( 'Search Testimonials' ),
		'not_found'				=> ( 'No testimonials found' ),
		'not_found_in_trash'	=> ( 'No testimonials found in trash' ),
		'parent_item_colon'		=> '',
		'menu_name'				=> 'Testimonials'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our testimonials and testimonial specific data',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'   => true,
	);
	register_post_type( 'testimonials', $args );
}
add_action('init', 'my_custom_post_testimonials');

function my_custom_post_gallery() {
	$labels = array(
		'name' 					=> _x( 'Gallery', 'post type general name'),
		'singular_name' 		=> _x( 'Image', 'post type singular name'),
		'add_new' 				=> _x( 'Add New Image', 'book'),
		'add_new_item'			=> ( 'Add New Image' ),
		'edit_item'				=> ( 'Edit Image' ),
		'new_item'				=> ( 'New Image' ),
		'all_items'				=> ( 'All Images' ),
		'view_item'				=> ( 'View Image' ),
		'search_items'			=> ( 'Search Images' ),
		'not_found'				=> ( 'No images found' ),
		'not_found_in_trash'	=> ( 'No images found in trash' ),
		'parent_item_colon'		=> '',
		'menu_name'				=> 'Gallery'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Holds our imagess and gallery specific data',
		'public'        => true,
		'menu_position' => 6,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'   => true,
	);
	register_post_type( 'gallery', $args );
}
add_action('init', 'my_custom_post_gallery');
add_action('init', 'flush_rewrite_rules');

//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);
?>