<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="http://fast.fonts.net/jsapi/0e0aba53-0436-496b-aaed-4f54eb7ccc3c.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title(); ?></title>
<?php wp_head(); ?>
</head>
<body>
    <header>
    	<div class="container">
            <h1><a href="#title"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php wp_title(); ?>"></a></h1>
            <nav>
                <ul>
                    <li><a href="#about" title="About">About</a></li>
                    <li><a href="#portfolio" title="Portfolio">Portfolio</a></li>
                    <li><a href="#contact" title="Contact">Contact</a></li>
                </ul>
            </nav>
            <div class="clear"></div>
        </div>
    </header>
    <section id="title">
    	<div class="container">
    		<div class="large-title"></div>
        </div>
    </section>
    <section id="responsive-title">
    	<div class="container">
        	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sales-message.png" class="sales-message">
        </div>
    </section>
    <section id="about">
        <div class="container">
        	<div class="col main-content">
            	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <?php the_content();?>
                    <?php endwhile; else: ?>
                    	<h2>Something's not right here</h2>
                		<p>Head back to the <a href="<?php bloginfo('url'); ?>">Homepage</a> and try again</p>
                <?php endif; ?>
            </div>
            <div class="col red-line"></div>
            <div class="col testimonial">
            	<?php
					$queryObject = new WP_Query( 'post_type=testimonials&posts_per_page=1&orderby=rand' );
					// The Loop!
					if ($queryObject->have_posts()) {
				?>
				<?php
					while ($queryObject->have_posts()) {
					$queryObject->the_post();
				?>	
                <?php the_content(); ?>
                <p><span class="quote-author"><?php the_title(); ?></span></p>
                <?php
					}
				?>
				<?php
					}
				?>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    <section id="portfolio">
    	<ul class="bxslider">
        	<?php
                $queryObject = new WP_Query( 'post_type=gallery&posts_per_page=150&orderby=date&order=rand' );
                // The Loop!
                if ($queryObject->have_posts()) {
            ?>
            <?php
                while ($queryObject->have_posts()) {
                $queryObject->the_post();
            ?>
            <?php $background = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), 'full' ); ?>
            <li>
            	<div class="gallery-main lazy" style="background-image: url('<?php echo $background[0]; ?>');">
            </li>        
            <?php
                }
            ?>
            <?php
                }
            ?>
        </ul>
    </section>
    <section id="contact">
        <div class="container">
        	<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
            <div class="clear"></div>
        </div>
    </section>
    <footer>
        <div class="container">
        	<div class="footer-left">
            	<a href="https://www.facebook.com/BethTakingPhotography" target="_blank" title="Join me on Facebook">Join me on Facebook</a><span> | </span><a href="mailto:hello@bethtakingphotography.co.uk" title="Email me">Email me</a><span> | </span>+44 (0) 7807 589 315
            </div>
            <div class="footer-right">
            	<a href="http://thetwentysix.co.uk/" target="_blank" title="Website by The Twenty Six">Website by The Twenty Six</a>
            </div>
            <div class="clear"></div>
        </div>
    </footer>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.lazyload.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.bxslider.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){	
	//.parallax(xPosition, speedFactor, outerHeight) options:
	//xPosition - Horizontal position of the element
	//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
	//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
	$('#title').parallax("50%", 0.5);
	$('.large-title').parallax("50%", -0.3);
	$('.bxslider').bxSlider({
	  pager: false
	});
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
					  		scrollTop: target.offset().top - 80
						}, 1000);
					return false;
				}
			}
		});
	});	
})
</script>
<?php wp_footer() ?>
</body>
</html>