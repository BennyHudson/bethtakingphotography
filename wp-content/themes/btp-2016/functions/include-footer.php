<?php
	function tastic_footer() {
		echo 'Fueled by <a href="http://www.wordpress.org" target="_blank" style="color: #e6007e; font-weight: bold;">WordPress</a> | Designed, Built & Maintained by <a href="http://brandtastic.co.uk" style="color: #e6007e; font-weight: bold;" target="_blank">Brandtastic</a>';
	}
?>
