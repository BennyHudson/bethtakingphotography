			<footer>
				<section class="footer-contact">
					<section class="container">
						<p>Turn your wedding dream into a reality</p>
						<a href="#" class="contact-trigger">Contact Us</a>
					</section>
				</section>
				<section class="footer-main">
					<section class="container">
						<aside>
							<nav>
								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '' ) ); ?>	
							</nav>
						</aside>
						<aside>
							<p><span>Copyright &copy; <?php echo date("Y") ?> BethTakingPhotography.</span> <span>Website by <a href="http://ben-hudson.co.uk">The Twenty Six</a></span></p>
						</aside>
					</section>
				</section>
			</footer>
		</section>
	</section>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">

	<!--Google Analytics-->

	<?php wp_footer(); ?>
</body>
</html>
