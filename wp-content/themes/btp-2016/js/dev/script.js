$(document).ready(function() {

	jQuery('.container').fitVids();

	var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

    $('.contact-button a, .contact-trigger').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'hidden');
        $('.overlay-cover, .overlay-wrap').show().css('visibility', 'visible');
    });

    $('.overlay-close').click(function(e) {
        e.preventDefault();
        $('html').css('overflow', 'auto');
        $('.overlay-cover, .overlay-wrap').hide();
    });

    jQuery('#testimonial-slider').bxSlider({
        pager: false,
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        adaptiveHeight: true
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
