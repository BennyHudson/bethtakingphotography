<?php get_header(); ?>

	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-feature' ); ?>

	<section class="container ultra no-top">
		<article>
			<div class="article-feature" style="background: url('<?php echo $image[0]; ?>'); ?>');">
				<div class="colour-overlay">
					<div class="article-title">
						<div class="angle tl">
							<div class="angle tr">
								<div class="angle bl">
									<div class="angle br">
										<div class="title-content">
											<h1 class="post-title"><?php the_title(); ?></h1>
											<p class="wedding-meta"><span><i class="fa fa-calendar"></i> <?php the_field('wedding_date'); ?></span> <span><i class="fa fa-map-marker"></i> <?php the_field('wedding_location'); ?></span></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="article-body">
				<?php if(get_field('intro_content')) { ?>
					<div class="intro-content">
						<p><?php the_field('intro_content'); ?></p>
					</div>
				<?php } ?>
				<?php while(have_rows('wedding_builder')) { ?>
					<?php the_row(); ?>
					<?php if(get_row_layout('gallery')) { ?>
						<?php if(get_sub_field('wedding_gallery')) { ?>
							<div class="gallery-block">
								<?php while(the_repeater_field('wedding_gallery')) { ?>
									<?php

										$image_id = get_sub_field('image');

										$thumbnail = wp_get_attachment_image_src($image_id, 'wedding-thumb');
										$thumbnail_url = $thumbnail[0];

										$full = wp_get_attachment_image_src($image_id, 'full');
										$full_url = $full[0];

									?>
									<div>
										<img src="<?php echo $thumbnail_url; ?>" alt="<?php the_title(); ?>">
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					<?php } ?>
					<?php if(get_row_layout('content_block')) { ?>
						<?php if(get_sub_field('content')) { ?>
							<div class="content-block">
								<?php the_sub_field('content'); ?>
							</div>
						<?php } ?>
					<?php } ?>
					<?php if(get_row_layout('testimonial')) { ?>
						<?php if(get_sub_field('testimonial')) { ?>
							<div class="testimonial-block">
								<aside class="testimonial-image">
									<?php 
										$image_id = get_sub_field('testimonial_image'); 
										$thumbnail = wp_get_attachment_image_src($image_id, 'gallery-thumb');
										$thumbnail_url = $thumbnail[0];
									?>
									<img src="<?php echo $thumbnail_url; ?>" alt="<?php the_title(); ?>">
								</aside>
								<aside class="testimonial-content">
									<h3>What they say...</h3>
									<?php the_sub_field('testimonial'); ?>
								</aside>
							</div>
						<?php } ?>
					<?php } ?>
					<?php if(get_row_layout('large_image')) { ?>
						<?php if(get_sub_field('big_image')) { ?>
							<?php 
								$image_id = get_sub_field('big_image'); 
								$thumbnail = wp_get_attachment_image_src($image_id, 'wedding-large');
								$thumbnail_url = $thumbnail[0];
							?>
							<img src="<?php echo $thumbnail_url; ?>" alt="<?php the_title(); ?>" class="big-image">
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</section>
		</article>
	</section>

<?php get_footer(); ?>
