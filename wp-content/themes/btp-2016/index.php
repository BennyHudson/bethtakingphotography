<?php get_header(); ?>

	<?php if (have_posts()) { ?>
		<?php $count = 0; ?>
		<section class="container blog-top main no-top">

				<?php while ( have_posts() ) { ?>

					<?php 
						$count++;
						the_post(); 
					?>

					<?php if($count == 1) { ?>
						<aside class="article-thumb feature-blog">
							<aside class="feature-thumb">
								<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('wedding-thumb'); ?></a>
							</aside>
							<aside class="feature-excerpt">
								<div>
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<p class="wedding-meta"><span><i class="fa fa-calendar"></i> <?php the_field('wedding_date'); ?></span> <span><i class="fa fa-map-marker"></i> <?php the_field('wedding_location'); ?></span></p>
									<?php the_field('intro_content'); ?>
									<a href="<?php the_permalink(); ?>">Read More</a>
								</div>
							</aside>
						</aside>
						<div>
					<?php } else { ?>
						<aside class="article-thumb">
							<a href="<?php the_permalink(); ?>">
								<div class="thumbnail-overlay">
									<div class="overlay-content">
										<h2><?php the_title(); ?></h2>
										<p class="wedding-meta"><span><i class="fa fa-calendar"></i> <?php the_field('wedding_date'); ?></span></p>
									</div>
								</div>
								<?php the_post_thumbnail('gallery-thumb'); ?>
							</a>
						</aside>
					<?php } ?>

				<?php } ?>
			</div>
		</section>
	<?php } ?>

<?php get_footer(); ?>
