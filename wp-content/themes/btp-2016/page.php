<?php get_header(); ?>

	
	<?php if(is_page(7)) { ?>

		<section class="about-intro">
			<section class="container main">
				<div class="content-box">
					<div class="angle tl">
						<div class="angle tr">
							<div class="angle bl">
								<div class="angle br">
									<div class="about-content">
										<h1 class="page-title"><?php the_title(); ?></h1>
										<?php the_content(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>

		<?php 
		    $args = array(
		        'post_type'             => 'testimonials',
		        'posts_per_page'        => -1,
		        'orderby'               => 'rand'
		    );  
		    $the_query = new WP_Query( $args );
		?>
		<?php if($the_query->have_posts() ) { ?>
			<section class="testimonial-area">
				<section class="container main">
					<h2 class="section-title white">Happy People</h2>
					<ul id="testimonial-slider">
						<?php while($the_query->have_posts()) { ?>
							<?php $the_query->the_post(); ?>
								<li>
									<aside class="testimonial-image">
										<div class="testimonial-image-clip">
											<?php the_post_thumbnail('gallery-thumb'); ?>
										</div>
									</aside>
									<aside class="testimonial-content">
										<div>
											<?php the_content(); ?>
											<h2><?php the_title(); ?></h2>
										</div>
									</aside>
								</li>
					    	<?php wp_reset_postdata(); ?>
						<?php } ?>
					</ul>
				</section>
			</section>
		<?php } ?>


	<?php } ?>


	<?php if(get_field('the_team')) { ?>
		<section class="the-team">
			<section class="container ultra">
				<h2 class="section-title">The Team</h2>
				<div class="team-list">
					<?php while(the_repeater_field('the_team')) { ?>
						<aside class="team-member">
							<?php
								$image_id = get_sub_field('photo');

								$thumbnail = wp_get_attachment_image_src($image_id, 'gallery-thumb');
								$thumbnail_url = $thumbnail[0];
							?>
							<div class="photo-clip">
								<img src="<?php echo $thumbnail_url; ?>" alt="<?php the_sub_field('name'); ?>">
							</div>
							<div class="clear"></div>
							<h2><?php the_sub_field('name'); ?></h2>
							<?php the_sub_field('bio'); ?>
						</aside>
					<?php } ?>
				</div>
			</section>
		</section>
	<?php } ?>

<?php get_footer(); ?>
