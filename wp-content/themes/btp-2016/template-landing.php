<?php
	//Template Name: Landing Page
	get_header();
?>

	<section class="home-feature">

	</section>

	<section class="home-intro">

	</section>

	<?php 
	    $args = array(
	        'post_type'             => 'testimonials',
	        'posts_per_page'        => -1,
	        'orderby'               => 'rand'
	    );  
	    $the_query = new WP_Query( $args );
	?>
	<?php if($the_query->have_posts() ) { ?>
		<section class="testimonial-area">
			<section class="container main">
				<h2 class="section-title white">Happy People</h2>
				<ul id="testimonial-slider">
					<?php while($the_query->have_posts()) { ?>
						<?php $the_query->the_post(); ?>
							<li>
								<aside class="testimonial-image">
									<div class="testimonial-image-clip">
										<?php the_post_thumbnail('gallery-thumb'); ?>
									</div>
								</aside>
								<aside class="testimonial-content">
									<div>
										<?php the_content(); ?>
										<h2><?php the_title(); ?></h2>
									</div>
								</aside>
							</li>
				    	<?php wp_reset_postdata(); ?>
					<?php } ?>
				</ul>
			</section>
		</section>
	<?php } ?>

	<section class="home-feed">
		<section class="container ultra">
			<h2 class="section-title">Latest Work</h2>
			<div>
				<?php 
					$postslist = get_posts('numberposts=4');
				    foreach ($postslist as $post) {
				?>
					<?php setup_postdata($post); ?>
						<aside class="article-thumb">
							<a href="<?php the_permalink(); ?>">
								<div class="thumbnail-overlay">
									<div class="overlay-content">
										<h2><?php the_title(); ?></h2>
										<p class="wedding-meta"><span><i class="fa fa-calendar"></i> <?php the_field('wedding_date'); ?></span></p>
									</div>
								</div>
								<?php the_post_thumbnail('gallery-thumb'); ?>
							</a>
						</aside>
					<?php wp_reset_postdata(); ?>	
				<?php } ?>
			</div>
		</section>
	</section>

<?php get_footer(); ?>
