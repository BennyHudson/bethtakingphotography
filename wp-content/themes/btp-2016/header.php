<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-custom.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/matchMedia.addListener.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/enquire.min.js" type="text/javascript"></script>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->

</head>
<body <?php body_class('tastic'); ?>>

	<div class="overlay-cover"></div>
	<div class="overlay-wrap" id="contact-us">
		<div class="overlay-container">
			<div class="overlay-content">
				<a href="#" class="overlay-close"><i class="fa fa-times-circle fa-2x"></i></a>
				<?php echo do_shortcode('[contact-form-7 id="153" title="Get in Touch"]'); ?>
			</div>
		</div>
	</div>

	<section class="page-content">
		<section class="footer-fix">

			<header>
				<section class="container">
					<aside class="header-logo">
						<?php get_template_part('snippets/content', 'logo'); ?>
					</aside>
					<aside class="header-main">
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => '' ) ); ?>
						</nav>
					</aside>
				</section>
			</header>
